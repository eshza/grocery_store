package grocerystoreproject;

/**
 *
 * @author jamie
 */
public class Staff{
	private String id;
	private String name;
	private int salary;
	private String job_title;
	private Address address;

	public String getID(){
		return id;
	}

	public void setID(String id){
		this.id = id;
	}

	public String getName(){
		return name;
	}

	public String setName(String name){
		this.name = name;
	}

	public int getSalary(){
		return salary;
	}

	public int setSalary(int sal){
		this.sal = salary;
	}

	public String getJobTitle(){
		return job_title;
	}

	public String setJobTitle(String jb){
		this.jb = job_title;
	}

	public Address getAddress(){
		return address;
	}

	public void setAddress(Address addr){
		this.addr = address;
	}

	
}