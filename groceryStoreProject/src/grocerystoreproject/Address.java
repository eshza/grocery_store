package grocerystoreproject;

/**
 *
 * @author jamie
 */
public class Address{
	private String id;
	private String addr_type;
	private String street_name;
	private String city;
	private String zipcode;
	private String state;

	public String getID(){
		return id;
	}

	public void setID(String add_id){
		this.add_id = id;
	}

	public String getType(){
		return addr_type;
	}

	public void setType(String type){
		this.type = addr_type;
	}

	public String getStreet(){
		return street_name;
	}

	public void setStreet(String strt){
		this.strt = street_name;
	}

	public String getCity(){
		return city;
	}

	public void setCity(String cty){
		this.cty = city;
	}

	public String getState(){
		return state;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getZipcode(){
		return zipcode;
	}

	public void setZipcode(String zip){
		this.zip = zipcode;
	}

}