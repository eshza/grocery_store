/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grocerystoreproject;
import java.sql.Connection;
import java.sql.DriverManager;
/**
 *
 * @author eshna
 */
public class DatabaseConnection {
    
//public static void main(String[] args){
//    DatabaseConnection dc = new DatabaseConnection();
//    dc.connectDatabase();
//}
static Connection c; 
public static Connection connectDatabase(){
    
    
    try{
        Class.forName("org.postgresql.Driver");
        c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/grocery_db", "postgres", "cs425");
        System.out.println("Opened database sucessfully");
        
    } catch (Exception e) {
        e.printStackTrace();
        System.err.println(e.getClass().getName()+": "+e.getMessage());
        System.exit(0);
        
    }
    return c;
    
}
}
