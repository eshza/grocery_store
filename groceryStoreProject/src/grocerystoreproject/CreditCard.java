package grocerystoreproject;

/**
 *
 * @author jamie
 */
public class CreditCard{
	private int card_no;
	private String expiry_date;
	private int cvv;
	private String userID;
	private String addressID;

	public int getCardNo(){
		return card_no;
	}

	public void setCardNo(int num){
		this.num = card_no;
	}

	public String getExpiry(){
		return expiry_date;
	}

	public void setExpiry(String expiry){
		this.expiry_date = expiry;
	}

	public int getCvv(){
		return cvv;
	}

	public void getCvv(int cv){
		this.cv = cvv;
	}

	public String getUser(){
		return userID;
	}

	public void setUser(String uid){
		this.uid = userID;
	}

	public String getAddress(){
		return addressID;
	}

	public void setAddress(String aid){
		this.aid = addressID;
	}

}