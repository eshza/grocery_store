package grocerystoreproject;

/**
 *
 * @author jamie
 */

 public class Warehouse {
 	private float storage_capacity;
 	private String warehouse_id;
 	private Address address;

 	public String getWarehouseID(){
 		return warehouse_id;
 	}

 	public float getStorageCapacity(){
 		return storage_capacity;
 	}

 	public void setStorageCapacity(float capacity){
 		this.storage_capacity = capacity;
 	}

 	public Address getAddress(){
 		return address;
 	}

 	public void setAddress(Address address){
 		this.address = address;
 	}
 }