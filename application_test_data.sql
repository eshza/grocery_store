-- address

INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (1,'warehouse_addr','Ap #350-4774 Nunc Rd.','Chami','MN','31962');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (2,'ccd_addr','8615 Nonummy St.','Cakewalk','MN','435673');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (3,'cust_pay_addr','295-1238 Aliquam Rd.','Chami','MN','22415-84855');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (4,'supplier_addr','299-606 Inceptos Av.','Altavista','MN','459366');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (5,'staff_addr','5030 Ligula. Avenue','Adobe','TN','4456');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (6,'supplier_addr','625-9769 Nibh. Rd.','Apple Systems','NV','T8E 7B9');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (7,'cust_del_addr','P.O. Box 212, 2026 Dolor St.','Macromedia','CA','Z8695');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (8,'cust_pay_addr','Ap #566-966 Elit. St.','Microsoft','TN','47124');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (9,'cust_pay_addr','7252 A St.','Macromedia','MN','263347');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (10,'cust_del_addr','P.O. Box 707, 1260 Dolor. Avenue','Chami','MN','6177');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (11,'cust_del_addr','789-7660 Mauris Avenue','Lycos','TN','5495');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (12,'cust_del_addr','Ap #497-3690 Ut, St.','Adobe','CA','P13 9JH');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (13,'ccd_addr','7034 Diam. Ave','Google','CA','Z5322');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (14,'staff_addr','P.O. Box 948, 5950 Augue Av.','Chami','CA','219420');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (15,'staff_addr','5215 Magna. Ave','Yahoo','MN','Z5201');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (16,'cust_pay_addr','P.O. Box 688, 357 Dolor Avenue','Google','CA','4543');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (17,'cust_pay_addr','Ap #152-4305 Molestie Ave','Altavista','NV','86718');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (18,'cust_del_addr','2459 At Avenue','Altavista','NV','472007');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (19,'cust_del_addr','Ap #893-2460 Fringilla Rd.','Cakewalk','CA','01192');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (20,'warehouse_addr','5326 Dolor, Ave','Sibelius','IL','WO1U 8DG');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (21,'warehouse_addr','9323 Eget, Ave','Finale','IL','814789');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (22,'cust_del_addr','P.O. Box 196, 8523 Ut, St.','Lycos','IL','68-195');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (23,'cust_del_addr','335-2690 Et St.','Altavista','TN','7320 ZB');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (24,'warehouse_addr','1890 Orci, Road','Sibelius','CA','97921');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (25,'supplier_addr','Ap #968-1255 Aliquet Avenue','Google','NV','8756');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (26,'ccd_addr','Ap #589-417 In Rd.','Adobe','NV','5278');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (27,'cust_del_addr','201-7847 Et, Rd.','Lycos','NV','Z9750');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (28,'warehouse_addr','P.O. Box 973, 270 Enim Road','Chami','IL','62518');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (29,'ccd_addr','848-825 Enim. St.','Finale','CA','1645');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (30,'ccd_addr','Ap #205-8516 Amet, St.','Chami','NV','39982');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (31,'ccd_addr','496-4559 Donec Ave','Borland','MN','96485');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (32,'cust_pay_addr','978-8453 Sed St.','Google','IL','2416');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (33,'warehouse_addr','P.O. Box 822, 9328 Nisi Rd.','Cakewalk','IL','S4L 1X8');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (34,'staff_addr','203-3816 Imperdiet Rd.','Borland','MN','Z1319');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (35,'staff_addr','Ap #789-8555 Eu Av.','Chami','CA','70-066');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (36,'supplier_addr','Ap #199-9861 Erat. Rd.','Yahoo','CA','63072');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (37,'staff_addr','179-6141 Mi Avenue','Cakewalk','MN','76174');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (38,'cust_del_addr','213-9975 Cras Rd.','Lavasoft','TN','68574');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (39,'cust_pay_addr','996-2944 Nostra, Road','Microsoft','IL','81998-781');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (40,'staff_addr','6961 Egestas, Rd.','Chami','NV','10398');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (41,'cust_del_addr','P.O. Box 824, 874 Nunc Road','Microsoft','IL','71704');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (42,'cust_pay_addr','P.O. Box 600, 7220 Feugiat Rd.','Lavasoft','TN','10279');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (43,'staff_addr','Ap #451-8226 Aliquet. Street','Apple Systems','TN','H65 5UC');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (44,'cust_pay_addr','360-9958 Neque St.','Google','IL','260492');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (45,'cust_pay_addr','5293 Aliquam Avenue','Cakewalk','IL','706354');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (46,'cust_pay_addr','7159 Semper St.','Lavasoft','TN','T2W 2R9');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (47,'staff_addr','P.O. Box 521, 155 Quam. St.','Finale','NV','6857');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (48,'staff_addr','671-2522 Sed St.','Microsoft','MN','20110');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (49,'staff_addr','P.O. Box 301, 1173 Mi Rd.','Microsoft','CA','8750');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (50,'cust_del_addr','Ap #218-3414 Leo Ave','Chami','CA','25432');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (51,'staff_addr','Ap #920-6312 Laoreet St.','Apple Systems','CA','8063');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (52,'staff_addr','2682 Lobortis, Av.','Macromedia','MN','K2N 2X7');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (53,'warehouse_addr','721-5748 Massa Ave','Lavasoft','TN','L8H 3N4');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (54,'cust_del_addr','Ap #715-4885 Mollis. Avenue','Adobe','NV','284394');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (55,'cust_del_addr','Ap #958-1482 Mi St.','Lavasoft','TN','38819');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (56,'ccd_addr','4391 Magna, Avenue','Lavasoft','NV','14284');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (57,'supplier_addr','916-4958 Vulputate, Ave','Lycos','TN','Z9321');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (58,'staff_addr','903-7484 Blandit. Rd.','Sibelius','CA','Z7821');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (59,'cust_pay_addr','208-4648 Vestibulum St.','Lycos','NV','854235');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (60,'staff_addr','Ap #405-998 Mauris St.','Apple Systems','NV','60282');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (61,'staff_addr','321-9735 Magna Road','Cakewalk','NV','8377');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (62,'warehouse_addr','Ap #376-9016 Nec, Rd.','Finale','IL','245907');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (63,'ccd_addr','516-4940 Cursus, Road','Finale','CA','77408');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (64,'cust_pay_addr','2340 Auctor Rd.','Lavasoft','NV','431159');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (65,'supplier_addr','P.O. Box 365, 6331 Ante Rd.','Yahoo','CA','4296');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (66,'staff_addr','854-1922 Semper Av.','Macromedia','IL','46739');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (67,'supplier_addr','5077 Luctus Av.','Adobe','TN','9786');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (68,'staff_addr','774-3044 Eu, Ave','Google','IL','L1G 5B6');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (69,'supplier_addr','510-781 Vehicula. Rd.','Lycos','MN','9116');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (70,'cust_del_addr','481-314 Diam. Ave','Apple Systems','CA','6382 ZP');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (71,'warehouse_addr','P.O. Box 828, 8631 Et Avenue','Finale','NV','60908');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (72,'supplier_addr','542-8568 Proin Avenue','Lycos','TN','2443');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (73,'supplier_addr','Ap #812-8089 Enim Ave','Sibelius','NV','445219');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (74,'staff_addr','2683 Blandit Rd.','Cakewalk','IL','Z0326');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (75,'ccd_addr','P.O. Box 463, 3781 Eu, Avenue','Altavista','CA','2893 HG');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (76,'ccd_addr','Ap #627-4597 Sem. Rd.','Altavista','NV','6318');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (77,'staff_addr','P.O. Box 392, 352 Mauris Road','Finale','IL','71313');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (78,'cust_pay_addr','P.O. Box 194, 9848 Est, Ave','Altavista','NV','41901');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (79,'supplier_addr','Ap #730-6588 Quisque St.','Lavasoft','MN','63917');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (80,'cust_del_addr','233-800 Et St.','Chami','NV','H7N 8T2');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (81,'staff_addr','273-6992 Sed Ave','Yahoo','NV','292318');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (82,'cust_pay_addr','9906 Curabitur Ave','Chami','CA','9349');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (83,'cust_del_addr','2141 Donec Road','Macromedia','MN','1794');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (84,'supplier_addr','103-7446 Nunc, Street','Lavasoft','MN','16-887');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (85,'staff_addr','5753 Inceptos Street','Adobe','MN','46-612');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (86,'warehouse_addr','P.O. Box 975, 5003 Scelerisque Road','Borland','TN','598477');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (87,'cust_del_addr','Ap #398-6662 Sem Ave','Sibelius','TN','441603');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (88,'warehouse_addr','608-2690 Orci Av.','Macromedia','MN','5103');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (89,'warehouse_addr','1404 At Street','Apple Systems','IL','80665');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (90,'warehouse_addr','878-7535 Pede. Rd.','Sibelius','TN','7208');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (91,'cust_pay_addr','551-5203 Ligula. Rd.','Google','MN','8550');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (92,'supplier_addr','P.O. Box 395, 8155 Aliquam Avenue','Google','NV','35-789');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (93,'supplier_addr','216-1277 Et Av.','Borland','MN','5300');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (94,'cust_del_addr','580-7197 Curae; St.','Lavasoft','CA','71698');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (95,'cust_del_addr','Ap #206-325 A St.','Cakewalk','MN','77770');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (96,'staff_addr','5228 Consectetuer St.','Google','CA','24430');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (97,'warehouse_addr','Ap #795-3846 Vulputate, Street','Lycos','CA','A0R 3NC');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (98,'staff_addr','Ap #730-1423 Nunc Road','Finale','CA','11500');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (99,'staff_addr','871-4992 Luctus. St.','Chami','CA','543498');
INSERT INTO address (address_id,address_type,street_name,city,state_name,zip_code) VALUES (100,'supplier_addr','Ap #202-2943 Dapibus Avenue','Sibelius','TN','6714');



-- product

INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (1,'cursus.','pasta','Vestibulum accumsan neque',87,475);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (2,'nonummy','desserts','enim nec tempus',92,86);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (3,'nibh','seafood','Sed auctor odio',60,359);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (4,'varius.','salads','et pede. Nunc',59,251);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (5,'mollis','sandwiches','nisi magna sed',77,284);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (6,'pede.','pasta','vulputate, risus a',94,444);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (7,'Sed','soups','lobortis risus. In',81,427);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (8,'magna.','pies','Curabitur dictum. Phasellus',2,387);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (9,'Sed','desserts','aliquam, enim nec',96,492);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (10,'dignissim.','pasta','facilisi. Sed neque.',71,315);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (11,'odio','noodles','Morbi accumsan laoreet',99,208);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (12,'elementum,','seafood','porttitor interdum. Sed',88,476);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (13,'magna.','salads','non quam. Pellentesque',20,93);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (14,'lacus.','stews','porta elit, a',64,504);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (15,'erat','sandwiches','montes, nascetur ridiculus',18,385);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (16,'consectetuer','seafood','dictum ultricies ligula.',48,243);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (17,'lectus','soups','aliquam, enim nec',88,340);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (18,'cursus','stews','euismod est arcu',7,494);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (19,'ornare','salads','blandit. Nam nulla',30,207);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (20,'mi.','noodles','posuere at, velit.',24,384);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (21,'nec,','stews','odio. Phasellus at',48,167);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (22,'vitae','desserts','condimentum. Donec at',94,397);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (23,'neque','stews','dignissim. Maecenas ornare',82,317);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (24,'ipsum','soups','semper tellus id',60,423);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (25,'Aenean','cereals','auctor odio a',32,328);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (26,'urna','seafood','et, magna. Praesent',68,442);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (27,'libero.','soups','lacus pede sagittis',29,352);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (28,'enim.','soups','augue id ante',37,88);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (29,'sit','seafood','a mi fringilla',84,215);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (30,'enim.','pasta','neque vitae semper',70,154);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (31,'sit','sandwiches','venenatis vel, faucibus',86,115);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (32,'amet,','salads','nisi. Aenean eget',27,383);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (33,'velit.','pies','adipiscing, enim mi',42,111);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (34,'parturient','noodles','a, magna. Lorem',58,229);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (35,'Fusce','seafood','in consectetuer ipsum',13,189);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (36,'mollis','salads','rutrum non, hendrerit',55,197);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (37,'cursus','cereals','nisi. Mauris nulla.',60,354);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (38,'neque','sandwiches','et, eros. Proin',6,295);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (39,'purus','stews','malesuada fringilla est.',97,113);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (40,'eros','pies','quam quis diam.',70,130);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (41,'Cras','cereals','elit. Nulla facilisi.',59,116);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (42,'accumsan','noodles','Integer aliquam adipiscing',55,183);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (43,'posuere','desserts','fringilla purus mauris',18,488);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (44,'auctor','soups','vitae diam. Proin',75,235);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (45,'felis','noodles','quis accumsan convallis,',22,301);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (46,'Aenean','cereals','orci lobortis augue',43,247);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (47,'lacus.','salads','eget mollis lectus',73,310);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (48,'Suspendisse','seafood','dolor dapibus gravida.',84,339);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (49,'Duis','noodles','placerat, orci lacus',6,524);
INSERT INTO "product" (product_id,name,category,nutritional_info,alcohol_content,size) VALUES (50,'convallis','pies','semper. Nam tempor',46,272);



-- supplier

INSERT INTO "supplier" (supplier_id,name) VALUES (1,'Suspendisse Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (2,'Magna Consulting');
INSERT INTO "supplier" (supplier_id,name) VALUES (3,'Vestibulum Nec Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (4,'Convallis Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (5,'Non Enim Commodo Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (6,'Mauris Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (7,'Tortor Integer Aliquam Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (8,'Congue Turpis PC');
INSERT INTO "supplier" (supplier_id,name) VALUES (9,'Dictum Eu Placerat Incorporated');
INSERT INTO "supplier" (supplier_id,name) VALUES (10,'Semper Dui Lectus Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (11,'Quisque Ac Libero Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (12,'Lorem Inc.');
INSERT INTO "supplier" (supplier_id,name) VALUES (13,'Ipsum Dolor Sit LLP');
INSERT INTO "supplier" (supplier_id,name) VALUES (14,'Tempor PC');
INSERT INTO "supplier" (supplier_id,name) VALUES (15,'Velit Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (16,'Dolor Fusce Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (17,'Egestas Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (18,'Eget Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (19,'Ut Tincidunt Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (20,'Consectetuer Ipsum Nunc Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (21,'Et Inc.');
INSERT INTO "supplier" (supplier_id,name) VALUES (22,'Donec Porttitor Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (23,'Molestie Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (24,'Velit Eu Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (25,'Odio A Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (26,'Id Limited');
INSERT INTO "supplier" (supplier_id,name) VALUES (27,'Gravida Mauris Ut Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (28,'Sociis Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (29,'Dui LLC');
INSERT INTO "supplier" (supplier_id,name) VALUES (30,'Sagittis Placerat Limited');
INSERT INTO "supplier" (supplier_id,name) VALUES (31,'Faucibus Id Libero LLC');
INSERT INTO "supplier" (supplier_id,name) VALUES (32,'Augue Sed Molestie Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (33,'Commodo At Limited');
INSERT INTO "supplier" (supplier_id,name) VALUES (34,'Aliquet Metus Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (35,'Ante Lectus Convallis Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (36,'Purus Duis Elementum Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (37,'Curae; Phasellus LLP');
INSERT INTO "supplier" (supplier_id,name) VALUES (38,'Duis Dignissim Tempor LLC');
INSERT INTO "supplier" (supplier_id,name) VALUES (39,'Fringilla Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (40,'Neque Sed Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (41,'Nec Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (42,'Etiam PC');
INSERT INTO "supplier" (supplier_id,name) VALUES (43,'Suspendisse Commodo Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (44,'Feugiat Non Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (45,'Vitae Aliquam Eros LLP');
INSERT INTO "supplier" (supplier_id,name) VALUES (46,'Arcu Inc.');
INSERT INTO "supplier" (supplier_id,name) VALUES (47,'Cras Interdum Nunc Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (48,'Urna Consulting');
INSERT INTO "supplier" (supplier_id,name) VALUES (49,'Sed Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (50,'Nulla Magna Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (51,'Fermentum Fermentum Arcu Inc.');
INSERT INTO "supplier" (supplier_id,name) VALUES (52,'Eleifend Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (53,'Luctus Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (54,'Donec Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (55,'Sodales Purus In Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (56,'Arcu Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (57,'Accumsan Sed PC');
INSERT INTO "supplier" (supplier_id,name) VALUES (58,'Nec Tempus Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (59,'Neque Non Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (60,'Dolor LLC');
INSERT INTO "supplier" (supplier_id,name) VALUES (61,'Penatibus Et Magnis Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (62,'Duis PC');
INSERT INTO "supplier" (supplier_id,name) VALUES (63,'Donec Tempor Est Inc.');
INSERT INTO "supplier" (supplier_id,name) VALUES (64,'Elit Elit Fermentum Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (65,'Nulla Eu Neque Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (66,'Nibh Incorporated');
INSERT INTO "supplier" (supplier_id,name) VALUES (67,'Facilisis Non Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (68,'Adipiscing Non Ltd');
INSERT INTO "supplier" (supplier_id,name) VALUES (69,'Dictum LLC');
INSERT INTO "supplier" (supplier_id,name) VALUES (70,'Bibendum Donec Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (71,'At Auctor Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (72,'Sit Incorporated');
INSERT INTO "supplier" (supplier_id,name) VALUES (73,'Ligula Donec Consulting');
INSERT INTO "supplier" (supplier_id,name) VALUES (74,'Inceptos Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (75,'Et Eros Proin Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (76,'Quam Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (77,'A Mi Fringilla Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (78,'Sed Incorporated');
INSERT INTO "supplier" (supplier_id,name) VALUES (79,'Pede Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (80,'Proin Velit Corporation');
INSERT INTO "supplier" (supplier_id,name) VALUES (81,'Gravida Molestie Arcu PC');
INSERT INTO "supplier" (supplier_id,name) VALUES (82,'Aliquet Consulting');
INSERT INTO "supplier" (supplier_id,name) VALUES (83,'Amet Faucibus Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (84,'Penatibus Et Magnis Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (85,'Mus Aenean Eget Associates');
INSERT INTO "supplier" (supplier_id,name) VALUES (86,'Nulla Facilisi Sed Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (87,'In Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (88,'Eu Euismod Industries');
INSERT INTO "supplier" (supplier_id,name) VALUES (89,'Et Commodo Inc.');
INSERT INTO "supplier" (supplier_id,name) VALUES (90,'Dolor Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (91,'Phasellus Limited');
INSERT INTO "supplier" (supplier_id,name) VALUES (92,'Nam Tempor Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (93,'Sem Ut Dolor Institute');
INSERT INTO "supplier" (supplier_id,name) VALUES (94,'Phasellus Vitae Foundation');
INSERT INTO "supplier" (supplier_id,name) VALUES (95,'Non Dapibus LLC');
INSERT INTO "supplier" (supplier_id,name) VALUES (96,'Penatibus Company');
INSERT INTO "supplier" (supplier_id,name) VALUES (97,'Donec Sollicitudin Adipiscing Limited');
INSERT INTO "supplier" (supplier_id,name) VALUES (98,'Natoque Penatibus Et Corp.');
INSERT INTO "supplier" (supplier_id,name) VALUES (99,'Quis Accumsan Convallis LLP');
INSERT INTO "supplier" (supplier_id,name) VALUES (100,'Morbi Metus Associates');



-- products_carried

INSERT INTO "products_carried" (product_id,supplier_id) VALUES (1,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (2,5);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (3,5);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (4,6);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (5,3);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (6,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (7,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (8,5);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (9,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (10,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (11,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (12,5);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (13,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (14,5);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (15,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (16,6);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (17,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (18,6);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (19,3);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (20,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (21,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (22,3);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (23,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (24,6);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (25,6);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (26,3);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (27,6);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (28,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (29,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (30,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (31,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (32,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (33,3);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (34,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (35,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (36,5);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (37,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (38,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (39,3);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (40,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (41,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (42,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (43,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (44,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (45,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (46,1);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (47,4);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (48,2);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (49,7);
INSERT INTO "products_carried" (product_id,supplier_id) VALUES (50,3);



-- warehouse 

INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (1,3504);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (2,4542);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (3,1700);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (4,4905);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (5,1367);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (6,607);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (7,3768);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (8,2148);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (9,384);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (10,36);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (11,2181);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (12,3763);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (13,1875);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (14,2558);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (15,82);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (16,2395);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (17,3188);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (18,2141);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (19,456);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (20,2673);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (21,3050);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (22,3880);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (23,357);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (24,106);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (25,1172);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (26,4314);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (27,4872);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (28,3632);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (29,851);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (30,3174);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (31,4229);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (32,1382);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (33,4740);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (34,1023);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (35,474);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (36,1977);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (37,1594);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (38,2204);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (39,3840);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (40,225);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (41,4341);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (42,676);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (43,4165);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (44,1547);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (45,847);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (46,724);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (47,3804);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (48,3484);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (49,2724);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (50,51);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (51,3808);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (52,2585);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (53,4230);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (54,4809);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (55,4331);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (56,4203);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (57,227);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (58,4020);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (59,108);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (60,3272);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (61,2547);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (62,844);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (63,2782);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (64,243);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (65,3798);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (66,626);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (67,3901);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (68,3184);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (69,3055);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (70,2603);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (71,3246);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (72,3511);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (73,1505);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (74,2498);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (75,1571);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (76,3755);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (77,3481);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (78,495);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (79,4162);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (80,3721);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (81,1712);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (82,4644);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (83,4307);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (84,2842);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (85,1002);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (86,2205);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (87,3884);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (88,4966);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (89,4928);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (90,4499);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (91,1157);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (92,3358);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (93,4490);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (94,1160);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (95,1120);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (96,1053);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (97,1504);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (98,4685);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (99,1817);
INSERT INTO "warehouse" (warehouse_id,storage_capacity) VALUES (100,430);



-- stock

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,1,734);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,1,13);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,1,160);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,1,927);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,1,515);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,1,329);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,1,420);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,1,729);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,1,676);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,1,793);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,1,397);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,1,205);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,1,442);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,1,691);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,1,350);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,1,177);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,1,154);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,1,435);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,1,455);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,1,839);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,1,147);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,1,343);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,1,314);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,1,570);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,1,950);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,1,42);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,1,33);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,1,295);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,1,313);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,1,666);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,1,368);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,1,68);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,1,410);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,1,942);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,1,317);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,1,892);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,1,576);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,1,127);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,1,440);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,1,764);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,1,573);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,1,810);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,1,824);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,1,882);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,1,879);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,1,342);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,1,277);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,1,403);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,1,37);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,1,590);

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,2,596);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,2,433);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,2,511);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,2,990);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,2,266);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,2,370);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,2,898);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,2,415);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,2,7);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,2,323);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,2,348);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,2,232);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,2,937);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,2,235);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,2,803);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,2,164);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,2,996);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,2,936);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,2,118);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,2,675);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,2,94);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,2,649);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,2,554);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,2,674);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,2,384);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,2,72);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,2,161);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,2,949);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,2,682);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,2,783);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,2,403);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,2,98);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,2,302);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,2,630);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,2,34);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,2,437);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,2,708);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,2,68);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,2,757);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,2,757);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,2,573);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,2,577);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,2,677);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,2,376);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,2,909);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,2,236);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,2,767);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,2,917);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,2,339);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,2,741);

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,3,672);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,3,319);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,3,728);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,3,561);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,3,169);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,3,216);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,3,582);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,3,847);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,3,677);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,3,129);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,3,553);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,3,179);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,3,122);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,3,445);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,3,202);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,3,578);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,3,108);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,3,799);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,3,838);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,3,335);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,3,330);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,3,97);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,3,105);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,3,310);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,3,822);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,3,462);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,3,177);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,3,588);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,3,555);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,3,331);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,3,857);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,3,803);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,3,130);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,3,104);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,3,194);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,3,106);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,3,614);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,3,50);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,3,825);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,3,344);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,3,281);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,3,812);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,3,629);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,3,601);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,3,302);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,3,923);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,3,81);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,3,522);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,3,370);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,3,926);

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,4,334);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,4,140);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,4,230);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,4,656);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,4,486);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,4,426);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,4,415);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,4,301);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,4,73);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,4,97);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,4,947);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,4,12);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,4,318);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,4,771);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,4,654);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,4,800);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,4,197);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,4,275);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,4,913);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,4,165);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,4,886);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,4,783);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,4,242);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,4,961);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,4,803);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,4,102);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,4,7);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,4,181);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,4,208);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,4,431);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,4,302);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,4,517);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,4,782);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,4,113);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,4,384);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,4,100);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,4,348);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,4,381);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,4,733);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,4,902);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,4,807);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,4,252);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,4,752);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,4,856);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,4,267);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,4,263);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,4,803);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,4,711);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,4,954);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,4,2);

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,5,230);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,5,252);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,5,103);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,5,16);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,5,666);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,5,137);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,5,142);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,5,123);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,5,504);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,5,708);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,5,263);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,5,942);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,5,439);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,5,494);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,5,555);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,5,736);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,5,97);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,5,908);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,5,444);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,5,352);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,5,92);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,5,847);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,5,898);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,5,884);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,5,789);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,5,356);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,5,337);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,5,800);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,5,685);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,5,512);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,5,903);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,5,708);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,5,44);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,5,781);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,5,601);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,5,44);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,5,639);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,5,336);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,5,530);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,5,924);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,5,644);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,5,37);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,5,318);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,5,69);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,5,341);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,5,859);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,5,658);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,5,947);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,5,814);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,5,91);

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,6,782);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,6,562);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,6,590);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,6,58);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,6,148);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,6,614);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,6,475);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,6,643);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,6,967);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,6,572);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,6,593);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,6,726);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,6,113);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,6,186);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,6,940);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,6,31);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,6,613);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,6,149);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,6,682);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,6,497);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,6,772);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,6,757);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,6,962);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,6,62);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,6,68);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,6,788);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,6,359);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,6,71);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,6,672);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,6,173);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,6,614);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,6,408);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,6,211);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,6,599);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,6,840);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,6,715);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,6,530);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,6,760);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,6,178);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,6,990);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,6,416);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,6,944);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,6,410);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,6,349);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,6,645);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,6,713);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,6,72);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,6,121);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,6,122);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,6,334);

INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (1,7,585);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (2,7,876);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (3,7,459);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (4,7,722);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (5,7,663);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (6,7,131);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (7,7,188);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (8,7,536);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (9,7,203);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (10,7,476);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (11,7,254);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (12,7,169);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (13,7,644);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (14,7,423);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (15,7,467);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (16,7,760);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (17,7,698);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (18,7,649);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (19,7,752);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (20,7,759);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (21,7,771);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (22,7,468);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (23,7,666);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (24,7,964);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (25,7,702);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (26,7,893);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (27,7,219);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (28,7,285);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (29,7,782);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (30,7,181);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (31,7,916);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (32,7,230);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (33,7,663);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (34,7,307);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (35,7,924);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (36,7,900);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (37,7,689);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (38,7,872);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (39,7,493);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (40,7,827);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (41,7,343);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (42,7,287);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (43,7,863);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (44,7,868);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (45,7,551);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (46,7,467);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (47,7,477);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (48,7,866);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (49,7,714);
INSERT INTO "stock" (product_id,warehouse_id,squantity) VALUES (50,7,301);

 

					
					
					
-- staff

INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (1,'Alma Mejia',367089,'Sales and Marketing');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (2,'May Hickman',215022,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (3,'Ila Craft',405749,'Finances');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (4,'Howard Petersen',235221,'Public Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (5,'Daryl York',305154,'Media Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (6,'Aidan Camacho',223847,'Customer Service');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (7,'Astra Salazar',277043,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (8,'Brenda Levy',316571,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (9,'Knox Delacruz',188139,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (10,'Abra Conway',307092,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (11,'Jerome Serrano',409605,'Asset Management');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (12,'Miriam Humphrey',351541,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (13,'Ivan Cervantes',158952,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (14,'Ralph Bolton',178096,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (15,'Amal Horn',424883,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (16,'Octavia Blackwell',380807,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (17,'Bianca Compton',104828,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (18,'Charity Marshall',81977,'Sales and Marketing');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (19,'Wyoming Caldwell',152490,'Sales and Marketing');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (20,'Whoopi Martin',226175,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (21,'Carson Bell',361917,'Public Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (22,'Gemma Macias',224300,'Sales and Marketing');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (23,'Kessie Bradley',210435,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (24,'Graiden Andrews',202148,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (25,'Dorothy Dunlap',355925,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (26,'Iliana Saunders',82252,'Finances');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (27,'Dylan Woods',148092,'Customer Service');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (28,'Philip Holland',352973,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (29,'Iona Stone',376978,'Public Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (30,'Porter Franco',214576,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (31,'Jackson Oconnor',32620,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (32,'Faith Hensley',276509,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (33,'Barry Norton',222954,'Legal Department');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (34,'Leilani Fisher',166310,'Accounting');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (35,'Orson Phillips',490607,'Media Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (36,'Rogan Bowen',338998,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (37,'Rhonda Cox',385118,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (38,'Hannah Dyer',76674,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (39,'Leigh Bradford',141993,'Customer Service');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (40,'Griffin Osborn',98538,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (41,'Mufutau Elliott',357233,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (42,'Patrick Nielsen',52417,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (43,'Vanna Buchanan',276627,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (44,'Maggie Cervantes',112465,'Accounting');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (45,'Roary Baxter',88713,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (46,'Melyssa Lynch',247862,'Legal Department');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (47,'Salvador Harrison',274435,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (48,'Martena Griffith',252977,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (49,'Warren Moore',157176,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (50,'Deacon Howell',220734,'Public Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (51,'Diana Holloway',70477,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (52,'Olga Hays',279554,'Legal Department');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (53,'Colette Cleveland',63171,'Legal Department');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (54,'Cade James',444357,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (55,'Odysseus Sheppard',81071,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (56,'Colette Mayer',216647,'Legal Department');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (57,'Sydney Sexton',474338,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (58,'Tate Holt',427521,'Finances');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (59,'Quintessa Spence',225339,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (60,'Amir Vasquez',344720,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (61,'Rogan Hebert',142093,'Media Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (62,'Olympia Chavez',50360,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (63,'Josiah Hardin',409562,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (64,'Yvette Huffman',245256,'Asset Management');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (65,'Remedios Blankenship',379121,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (66,'Elijah Hicks',104175,'Accounting');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (67,'Hyacinth Lynn',490734,'Customer Service');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (68,'Macaulay Kaufman',53996,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (69,'Kyla Vasquez',489910,'Public Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (70,'Jane Pace',407741,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (71,'Michelle Conway',148143,'Media Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (72,'Jessamine Eaton',434832,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (73,'Lewis Ramirez',454467,'Media Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (74,'Harrison Cruz',252263,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (75,'Jacob Boone',90871,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (76,'Yardley Hinton',65260,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (77,'Ashton Ingram',201669,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (78,'Cheryl Glenn',379792,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (79,'Ignacia Moore',373650,'Asset Management');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (80,'Meredith Mann',127364,'Asset Management');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (81,'Yael Macdonald',453309,'Sales and Marketing');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (82,'Erasmus Noel',319382,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (83,'Thaddeus Snow',155697,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (84,'Jasper Murphy',289426,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (85,'Elvis Rush',122480,'Payroll');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (86,'Kylee Shields',144274,'Public Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (87,'Destiny Trevino',379036,'Advertising');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (88,'Peter Evans',222078,'Research and Development');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (89,'Timon Larson',384695,'Quality Assurance');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (90,'Patience Pittman',309563,'Tech Support');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (91,'Rhonda Moran',377751,'Human Resources');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (92,'Barry Cantu',324274,'Asset Management');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (93,'Megan Sweeney',413554,'Human Resources');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (94,'Suki Gonzalez',121727,'Finances');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (95,'Vivian Valdez',378973,'Customer Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (96,'Ryder Moon',273572,'Asset Management');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (97,'Wylie Weber',477242,'Sales and Marketing');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (98,'Kareem Johns',328933,'Accounting');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (99,'Zelda Mccarthy',86200,'Media Relations');
INSERT INTO "staff" (staff_id,name,salary,job_title) VALUES (100,'Unity Sullivan',263464,'Payroll');



-- customer

INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (1,'Deanna Clements',250);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (2,'Melinda Boyle',622);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (3,'Fulton Cardenas',754);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (4,'Joel Padilla',875);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (5,'Brian Munoz',559);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (6,'Madonna Moore',978);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (7,'Kirestin Kemp',768);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (8,'Martin Nguyen',581);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (9,'Zephr Mann',613);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (10,'Cecilia White',643);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (11,'Kameko Ayers',105);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (12,'Veda Mccormick',379);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (13,'Lynn Saunders',208);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (14,'Merritt Clayton',137);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (15,'Abra Mckenzie',569);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (16,'Kadeem Davis',308);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (17,'Renee Zamora',116);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (18,'Yardley Bradford',583);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (19,'Eleanor Workman',180);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (20,'Laura Kane',996);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (21,'Hilda Stephenson',612);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (22,'Halla Cleveland',918);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (23,'Ria Duffy',850);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (24,'Uma Curtis',38);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (25,'Uriah Mitchell',914);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (26,'Paki Woodward',205);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (27,'Shea Rivera',698);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (28,'Harding Irwin',22);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (29,'Giacomo Walton',867);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (30,'Blaine Hendrix',797);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (31,'Ima Castillo',2
8);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (32,'Maia Wiggins',284);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (33,'Ariel Sharpe',755);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (34,'Sybil Knapp',602);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (35,'Hashim Wells',602);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (36,'Blaze Pratt',538);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (37,'Steven Gilbert',784);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (38,'Jared Sampson',625);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (39,'Ivana Short',273);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (40,'Bell Coleman',832);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (41,'Jermaine Charles',501);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (42,'Ifeoma Bullock',997);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (43,'Leigh Guthrie',977);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (44,'Timon Kaufman',761);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (45,'Keaton Crosby',312);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (46,'Samuel Spence',992);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (47,'Phelan Ford',259);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (48,'Cassady Snyder',728);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (49,'Shelby Holder',824);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (50,'Aphrodite Coleman',161);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (51,'Yasir Lester',934);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (52,'Elvis Stevens',972);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (53,'Belle Wilkinson',652);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (54,'Nola Perry',99);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (55,'Adrian Richmond',711);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (56,'Mufutau Randall',999);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (57,'Brody Mcmillan',891);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (58,'Charity Trevino',496);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (59,'Hashim Leonard',604);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (60,'Cailin Cooper',802);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (61,'Levi Harding',357);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (62,'Thane Gonzalez',677);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (63,'Akeem Boyer',278);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (64,'Maggy Huffman',702);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (65,'Wylie Reid',469);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (66,'Quincy Carter',715);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (67,'Wesley David',117);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (68,'Murphy Wolfe',482);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (69,'Tara Collier',194);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (70,'Neville Woods',761);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (71,'Brenda Orr',958);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (72,'Ciaran Meyers',429);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (73,'Christopher Hamilton',629);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (74,'Aquila Hull',478);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (75,'Benjamin Harmon',435);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (76,'Drew Grant',832);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (77,'Hyacinth Stephens',261);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (78,'Samson Drake',998);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (79,'Zenia Kent',981);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (80,'Holly Thomas',500);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (81,'Chaim Lucas',793);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (82,'Ursa Ferguson',170);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (83,'Fletcher Rice',262);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (84,'Curran Hawkins',756);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (85,'Reuben Jacobson',118);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (86,'Byron Brady',59);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (87,'Imani Hardy',262);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (88,'Candace Ball',150);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (89,'Demetria Cooley',410);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (90,'Levi Russell',921);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (91,'Wylie Petty',716);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (92,'Hiroko Sampson',241);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (93,'Callie Watkins',830);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (94,'Rudyard Lawson',378);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (95,'Porter Nash',484);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (96,'Steven Alford',698);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (97,'Dora Woodard',214);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (98,'Amos Riddle',556);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (99,'Ayanna House',907);
INSERT INTO "customer" (customer_id,customer_name,account_balance) VALUES (100,'Guinevere Quinn',900);



-- order

INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (10,1,'09/03/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (62,2,'05/08/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (62,3,'09/05/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (15,4,'12/30/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (42,5,'02/19/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (84,6,'01/15/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (55,7,'06/20/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (2,8,'10/07/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (97,9,'01/04/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (18,10,'09/24/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (57,11,'02/13/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (74,12,'01/23/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (53,13,'09/10/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (40,14,'01/08/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (71,15,'02/05/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (69,16,'10/21/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (77,17,'04/07/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (6,18,'08/08/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (28,19,'10/11/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (79,20,'07/30/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (73,21,'03/09/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (84,22,'08/17/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (41,23,'06/07/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (4,24,'08/31/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (18,25,'10/19/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (8,26,'12/25/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (54,27,'07/28/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (30,28,'11/01/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (45,29,'04/09/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (86,30,'01/27/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (6,31,'05/02/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (47,32,'01/21/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (2,33,'11/01/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (90,34,'03/27/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (51,35,'09/16/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (98,36,'03/20/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (40,37,'08/18/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (58,38,'05/15/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (54,39,'12/21/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (45,40,'11/29/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (85,41,'08/18/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (42,42,'06/14/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (76,43,'08/16/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (85,44,'11/04/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (34,45,'04/26/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (84,46,'01/04/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (72,47,'04/10/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (58,48,'02/12/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (85,49,'07/22/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (42,50,'11/27/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (64,51,'10/05/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (39,52,'09/22/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (61,53,'07/24/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (76,54,'12/06/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (87,55,'03/07/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (48,56,'01/13/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (5,57,'10/20/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (84,58,'08/26/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (60,59,'10/26/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (59,60,'12/06/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (26,61,'08/19/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (17,62,'04/18/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (78,63,'03/30/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (14,64,'03/14/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (20,65,'03/15/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (50,66,'04/10/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (42,67,'03/26/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (46,68,'04/06/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (85,69,'05/28/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (51,70,'11/09/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (46,71,'10/22/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (82,72,'03/29/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (49,73,'04/06/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (84,74,'08/15/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (84,75,'09/03/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (80,76,'10/31/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (10,77,'01/21/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (75,78,'07/26/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (83,79,'10/07/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (95,80,'04/09/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (91,81,'08/17/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (88,82,'07/07/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (31,83,'06/08/20','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (82,84,'11/21/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (98,85,'06/01/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (30,86,'05/18/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (6,87,'04/02/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (29,88,'10/02/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (16,89,'07/11/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (90,90,'03/08/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (21,91,'04/15/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (22,92,'02/12/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (42,93,'04/23/21','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (47,94,'09/14/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (29,95,'08/16/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (11,96,'09/15/19','True');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (18,97,'07/04/19','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (88,98,'01/28/21','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (15,99,'09/18/20','False');
INSERT INTO "order" (customer_id,order_id,date_issued,status) VALUES (82,100,'09/28/20','False');



-- credit_card

INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (1,1,'4508371324399413','0221','352');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (2,2,'4913318073583340','0121','408');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (3,3,'4917759881834043','0720','988');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (4,4,'4175002166267101','0421','282');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (5,5,'4913822823629444','0520','540');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (6,6,'4508589727439722','1220','183');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (7,7,'4917046174011798','1120','954');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (8,8,'4844068245295737','0620','198');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (9,9,'4917143429993324','0321','652');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (10,10,'4026040540383005','0421','820');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (11,11,'4917435849824547','0620','682');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (12,12,'4917752415330570','0621','988');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (13,13,'4508201310089357','0720','562');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (14,14,'4844918878495550','1119','778');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (15,15,'4913048025715506','0920','989');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (16,16,'4844369534800272','1120','193');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (17,17,'4844026179031890','0320','719');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (18,18,'4913073027446371','0421','616');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (19,19,'4844973184341196','1220','221');
INSERT INTO "credit_card" (ccd_id,customer_id,card_number,exp_date,cvv) VALUES (20,20,'4508573337663954','0120','177');



-- entity_address

		-- first 20 are customers
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (1,7,1);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (2,8,2);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (3,10,3);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (4,11,4);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (5,16,5);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (6,17,6);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (7,19,7);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (8,27,8);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (9,32,9);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (10,38,10);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (11,41,11);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (12,42,12);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (13,50,13);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (14,55,14);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (15,78,15);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (16,82,16);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (17,83,17);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (18,91,18);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (19,94,19);
INSERT INTO "entity_address" (ent_addr_id,address_id, customer_id) VALUES (20,95,20);



		-- next 17 are staff members
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (21,5,1);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (22,15,2);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (23,34,3);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (24,37,4);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (25,40,5);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (26,47,6);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (27,48,7);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (28,49,8);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (29,51,9);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (30,58,10);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (31,60,11);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (32,61,12);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (33,66,13);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (34,74,14);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (35,77,15);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (36,96,16);
INSERT INTO "entity_address" (ent_addr_id,address_id,staff_id) VALUES (37,98,17);



		-- next 10 are suppliers
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (38,25,1);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (39,36,2);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (40,57,3);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (41,65,4);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (42,67,5);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (43,69,6);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (44,72,7);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (45,79,8);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (46,93,9);
INSERT INTO "entity_address" (ent_addr_id,address_id,supplier_id) VALUES (47,100,10);



		-- next 7 are warehouses
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (48,1,1);
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (49,24,2);
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (50,28,3);
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (51,71,4);
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (52,88,5);
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (53,89,6);
INSERT INTO "entity_address" (ent_addr_id,address_id,warehouse_id) VALUES (54,90,7);



		-- next are 8 credit cards
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (55,13,1);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (56,26,2);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (57,29,3);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (58,30,4);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (59,31,5);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (60,56,6);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (61,63,7);
INSERT INTO "entity_address" (ent_addr_id,address_id,ccd_id) VALUES (62,76,8);
		


-- state pricing

INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (1,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (2,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (3,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (4,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (5,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (6,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (7,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (8,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (9,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (10,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (11,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (12,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (13,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (14,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (15,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (16,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (17,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (18,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (19,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (20,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (21,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (22,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (23,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (24,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (25,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (26,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (27,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (28,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (29,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (30,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (31,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (32,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (33,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (34,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (35,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (36,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (37,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (38,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (39,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (40,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (41,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (42,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (43,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (44,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (45,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (46,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (47,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (48,'CA',3);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (49,'CA',2);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (50,'CA',2);

INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (1,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (2,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (3,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (4,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (5,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (6,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (7,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (8,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (9,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (10,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (11,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (12,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (13,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (14,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (15,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (16,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (17,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (18,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (19,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (20,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (21,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (22,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (23,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (24,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (25,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (26,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (27,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (28,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (29,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (30,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (31,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (32,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (33,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (34,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (35,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (36,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (37,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (38,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (39,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (40,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (41,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (42,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (43,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (44,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (45,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (46,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (47,'IL',5);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (48,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (49,'IL',4);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (50,'IL',5);

INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (1,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (2,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (3,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (4,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (5,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (6,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (7,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (8,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (9,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (10,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (11,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (12,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (13,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (14,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (15,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (16,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (17,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (18,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (19,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (20,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (21,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (22,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (23,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (24,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (25,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (26,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (27,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (28,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (29,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (30,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (31,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (32,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (33,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (34,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (35,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (36,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (37,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (38,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (39,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (40,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (41,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (42,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (43,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (44,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (45,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (46,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (47,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (48,'MN',7);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (49,'MN',6);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (50,'MN',7);

INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (1,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (2,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (3,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (4,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (5,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (6,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (7,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (8,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (9,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (10,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (11,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (12,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (13,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (14,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (15,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (16,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (17,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (18,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (19,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (20,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (21,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (22,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (23,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (24,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (25,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (26,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (27,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (28,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (29,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (30,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (31,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (32,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (33,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (34,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (35,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (36,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (37,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (38,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (39,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (40,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (41,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (42,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (43,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (44,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (45,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (46,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (47,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (48,'NV',9);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (49,'NV',8);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (50,'NV',9);

INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (1,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (2,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (3,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (4,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (5,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (6,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (7,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (8,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (9,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (10,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (11,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (12,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (13,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (14,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (15,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (16,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (17,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (18,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (19,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (20,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (21,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (22,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (23,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (24,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (25,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (26,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (27,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (28,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (29,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (30,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (31,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (32,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (33,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (34,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (35,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (36,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (37,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (38,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (39,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (40,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (41,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (42,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (43,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (44,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (45,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (46,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (47,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (48,'TN',11);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (49,'TN',10);
INSERT INTO "state_pricing" (product_id,state_name,multiplier) VALUES (50,'TN',10);




-- cart

	-- NOTE: cart table will be modified using "insert", "delete" and "update" statements (see application_sql_statements.sql)


-- delete rows that don't have an associated address or entity_address

delete
from credit_card
where customer_id in						
		((select customer_id
		from customer)
		except 
		(select customer_id
		from entity_address));

delete
from customer
where customer_id in						
		((select customer_id
		from customer)
		except 
		(select customer_id
		from entity_address));
				
delete
from staff
where staff_id in						
		((select staff_id
		from staff)
		except 
		(select staff_id
		from entity_address));
	
delete
from products_carried
where supplier_id in						
		((select supplier_id
		from supplier)
		except 
		(select supplier_id
		from entity_address));

delete
from supplier
where supplier_id in						
		((select supplier_id
		from supplier)
		except 
		(select supplier_id
		from entity_address));
		
delete
from stock
where warehouse_id in						
		((select warehouse_id
		from warehouse)
		except 
		(select warehouse_id
		from entity_address));		
		
delete
from warehouse
where warehouse_id in						
		((select warehouse_id
		from warehouse)
		except 
		(select warehouse_id
		from entity_address));
		
delete
from credit_card
where ccd_id in						
		((select ccd_id
		from credit_card)
		except 
		(select ccd_id
		from entity_address));		