drop table customer;
drop table address;
drop table entity_address;
drop table credit_card;
drop table staff;
drop table warehouse;
drop table product;
drop table state_pricing;
drop table orders;
drop table stock;
drop table supplier;
drop table cart;

create table customer(
    customer_id varchar(8),
    customer_name varchar(32) not null,
    account_balance numeric(8,2),
    primary key (customer_id)
);

create table credit_card(
    ccd_id varchar(8),
    customer_id varchar(8),
    card_number char(16) not null,
    exp_date char(4) not null,
    cvv char(4) not null,
    primary key (ccd_id),
    foreign key (customer_id) references customer on delete cascade

    
);

create table staff(
    staff_id varchar(8) not null,
    name varchar(20),
    salary integer,
    job_title varchar(20),
    primary key (staff_id)
    
);

create table product(
    product_id varchar(8) not null,
    product_name varchar(30),
    category varchar(15),
    nutritional_info varchar(50),
    alcohol_content numeric(5,2),
    product_size numeric (8,2),
    product_price numeric (8,2),
    primary key (product_id)
);

create table state_pricing(
	product_id varchar(8) not null, 
	state_name CHAR(2) not null, 
	multiplier numeric(8,2) not null,
	foreign key (product_id) references product on delete cascade,
	primary key (product_id, state_name)
	
);

create table cart (
	customer_id varchar(8),
	product_id varchar(8),
	quantity integer,
    unit_price numeric (8,2),
    total_price numeric (8,2),
	primary key (customer_id, product_id),
    foreign key (customer_id) references customer on delete cascade,
    foreign key (product_id) references product
);

create table supplier(
	product_id varchar(8),
    supplier_id varchar(8),
    supplier_name varchar(32) not null,
	foreign key (product_id) references product,
    primary key (supplier_id)
);

create table warehouse(
    warehouse_id varchar(8) not null ,
    storage_capacity integer,
    primary key (warehouse_id)
);

create table address(
    address_id varchar(8),
    address_type varchar (15) 
        check (address_type in ('cust_del_addr', 'cust_pay_addr', 'staff_addr', 'warehouse_addr', 'supplier_addr')),
    street_name varchar (32),
    city varchar(16),
    state_name char(2) not null,
    zip_code char(5),
    primary key (address_id)

);

create table entity_address(
    ent_addr_id varchar(8),
    address_id varchar(8) not null,
    customer_id varchar(8),
    staff_id varchar(8),
    supplier_id varchar(8),
    warehouse_id varchar(8),
    ccd_id varchar(8),

    primary key (ent_addr_id),
    foreign key (address_id) references address on delete cascade,
    foreign key (customer_id) references customer on delete cascade,
    foreign key (staff_id) references staff on delete cascade,
    foreign key (supplier_id) references supplier on delete cascade,
    foreign key (warehouse_id) references warehouse on delete cascade,
    foreign key (ccd_id) references credit_card

);


create table orders(
   customer_id varchar(8),
   order_id varchar(8),
   product_id varchar (8) not null,
   ccd_id varchar(8) not null, 
   date_issued char(10),
   status varchar(15) check (status in ('issued', 'send', 'recieved')), 
   primary key (customer_id, order_id),
   foreign key (customer_id) references customer on delete cascade,
   foreign key (product_id) references product,
   foreign key (ccd_id) references credit_card
);


create table stock(
    product_id varchar(8),
	warehouse_id varchar(8),
    squantity integer, 
    primary key (product_id, warehouse_id),
	foreign key (product_id) references product,
	foreign key (warehouse_id) references warehouse on delete cascade
);



create table products_carried(
	product_id varchar(8),
    product_price numeric (8,2), 
	supplier_id varchar(8),
	primary key (product_id, supplier_id),
	foreign key (product_id) references product,
	foreign key (supplier_id) references supplier on delete cascade
);


