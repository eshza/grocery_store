--4.1 Searching for products and placing orders

			--Customers can search for products available in their home state (delivery address) and browse through the catalog of such products grouped by product type.
select *
from product
where product_id in(
	select product_id
	from delivery_address_state
	where address_id in (
						select address_id
						from address
						where state_name in (
											select state_name
											from address
											where (address_type = 'cust_del_addr') and (address_id in (
																										select address_id
																										from entity_address
																										where customer_id = '7'   -- insert the customer's customer_id here
																									   )
																									   ) 
											)
							
						)
					)
order by category;



		--The application should maintain a shopping cart for a customer that stores the items the customer has selected so far and their quantity.
		--Customers can add and delete items from the shopping cart and change an items quantity.
insert into cart(customer_id, product_id, quantity, unit_price, total_price)
values
	('1', '1', 1, 10, 20);  -- insert user input in each of the spaces

delete
from cart
where customer_id = '1' and product_id = '1'; -- insert customer's customer_id ; insert user input for product_id

update cart
set quantity = 2  -- insert the customer's updated purchase quantity
where customer_id = '1' and product_id = '1';  -- insert customer's customer_id here ; also insert the product_id of the product whose quantity is being changed


		--Once a customer is satisfied with the shopping cart content, they can submit an order to order all the items that are currently in their shopping cart.


		--For each order, the customer can select one of the existing payment methods (credit cards).
select ccd_id, card_number, exp_date, cvv
from credit_card
where customer_id = '7';  -- insert the customer's customer_id here


		--Once an order has been placed, the total cost of the order is added to the customer's account balance.
		--the available quantity of products in the warehouse in the customers state should be reduced accordingly.

update customer
set account_balance = (account_balance + 10)  -- insert the total cost of the order here
where customer_id = '7';  -- insert customer's customer_id here

update stock
set squantity = (squantity - 10)  -- insert the quantity of the item ordered here
where product_id = '2' and warehouse_id in (  -- insert the product that the customer is ordering here
											select warehouse_id
											from entity_address
											where address_id in (  -- insert product_id of product being purchased ;  verify that the warehouse is in the customer's geographic area
																select address_id
																from address
																where (address_type = 'warehouse_addr') and (state_name = 'CA')  -- insert the state_name that belongs to the customer
																)
											);  




--4.2 Add/Delete/Modify a credit card and addresses

insert into credit_card(ccd_id, customer_id, card_number, exp_date, cvv)
values
	('100', '7', 0000000000000000, 1220, 123);  -- insert user input in each of the spaces
			
insert into address(address_id, address_type, street_name, city, zip_code)
values
	('101', 'cust_pay_addr', '123 Easy St.', 'Chicago', 12345);  -- insert user input in each of the spaces  ;  NOTE: address_type must always be 'cust_pay_addr' !


delete
from credit_card
where product_id = 51; -- insert user input for address_id

delete
from address
where (address_type ='cust_pay_addr') and address_id = 51; -- insert user input for address_id


	
update credit_card
set card_number = 0000000000000000 -- insert user input here
where (ccd_id = '7') and (customer_id = '7');

update credit_card
set exp_date = 0000  -- insert user input here
where (ccd_id = '7') and (customer_id = '7');

update credit_card
set cvv = 000  -- insert user input here
where (ccd_id = '7') and (customer_id = '7');


update address
set street_name = '543 Test Blvd.' -- insert user input here
where (address_type ='cust_pay_addr') and address_id = 51; -- insert user input for address_id

update address
set city = 'Denver'  -- insert user input here
where (address_type ='cust_pay_addr') and address_id = 51; -- insert user input for address_id

update address
set state_name = 'NV'  -- insert user input here
where (address_type ='cust_pay_addr') and address_id = 51; -- insert user input for address_id

update address
set zip_code = 54321  -- insert user input here
where (address_type ='cust_pay_addr') and address_id = 51; -- insert user input for address_id



--4.3 Add/Delete/Modify a products and product pricing

insert into product(product_id, name, category, nutritional_info, alcohol_content, size)
values(51, 'apple', 'fruit', 'eat one daily', 0, 93);   -- insert user input in each of the spaces

insert into product_pricing(product_id, state_multiplier, base_cost, product_price, supplier_price)
values(51, 1.5, 10, 30, 8);   -- insert user input in each of the spaces



			--NOTE: you must delete a row from product_pricing before you can delete the corresponding row from product
delete
from product_pricing
where product_id = 51; -- insert user input here
		
delete
from product
where product_id = 51;  -- insert user input here


		
		
update product
set name = 'gorganzola' -- insert new name of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product
set category = 'cheeses' -- insert new category of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product
set nutritional_info = 'soft cheese' -- insert new nutritional_info of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product
set alcohol_content = 0 -- insert new alcohol_content of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product
set size = 45 -- insert new size of product
where product_id = 51;  -- insert product_id of product that customer has selected


update product_pricing
set state_multiplier = 4.5
where product_id = 51;  -- insert product_id of product that customer has selected

update product_pricing
set state_multiplier = 4.5  -- insert new state_multiplier of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product_pricing
set base_cost = 15  -- insert new base_cost of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product_pricing
set product_price = 40  -- insert new product_price of product
where product_id = 51;  -- insert product_id of product that customer has selected

update product_pricing
set supplier_price = 10  -- insert new supplier_price of product
where product_id = 51;  -- insert product_id of product that customer has selected



--4.4 Manage stock

-- APPROACH 1

		-- for adding products to the warehouse
update stock
set squantity = (squantity + 10)  -- insert quantity being added
where product_id = '2' and warehouse_id = '2';  -- insert product_id and warehouse_id

-- for removing products from the warehouse
update stock
set squantity = (squantity - 10)  -- insert quantity being added
where product_id = '2' and warehouse_id = '2';  -- insert product_id and warehouse_id


--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- APPROACH 2

--add stock
insert into stock(item_id)
values(1234); --insert new stock id

insert into stock(item_id)
values(3456); --insert new stock id

insert into stock(item_id)
values(2345); --insert new stock id


--delete stock
delete 
from stock
where item_id = 2345; -- user input for stock to be deleted

delete 
from stock
where item_id = 1234; -- user input for stock to be deleted

delete 
from stock
where item_id = 3456; -- user input for stock to be deleted

--delete stocks that are null from stock and supplier
delete stock 
from stock as st, supplier as sp
where st.item_id = null;

--update stock
update stock
set item_id = 3456 --insert new id
where item_id = 1234; -id of stock to be updated

update stock
set item_id 1123--insert new id
where item_id = 1233; -id of stock to be updated

update stock
set item_id = 3476 --insert new id
where item_id = 6789; -id of stock to be updated






--4.1.1 BONUS: check availability
--check if item is in stock 
select product_id, item_id
from product as p, stock as s
where p.product_id = s.item_id; --if ids match, item is in stock





--4.1.2 BONUS: Save shopping carts between sessions

	
	
	
	
	
--4.3.1 BONUS: Product images
	
	
	
	
	
--4.4.1 BONUS: check storage limits

--check warehouse capacity
select storage_capacity
from warehouse
where warehouse_id = 123;

--compare to stock
select warehouse_id, count(s.item_id)
from warehouse as w, stock as s
where w.storage_capacity > count(s.item_id);
