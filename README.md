**CS 425 Grocery Store Project**

Authors:

Eshna Sengupta

Jamie Robles

Graham Halsey

---

## ER Diagram

The ER diagram can be found here: https://bitbucket.org/eshza/grocery_store/src/master/ER_DIAGRAM_CS425_3.png

---

## Relational Schema

The relational schema can be found here: https://bitbucket.org/eshza/grocery_store/src/master/grocery_schema.sql

---

## Application Demo

The grocery store application was built using Java and SQL supported through the netbeans framework. The app itself implements the following features: 

- Searching for products and placing orders

- Save shopping carts between sessions

- Add/Modify/Delete a credit card and addresses

- Add/Modify/Delete products and product pricing

- Manage stock

The project demo video can be found here: https://youtu.be/UESsenyt_uE